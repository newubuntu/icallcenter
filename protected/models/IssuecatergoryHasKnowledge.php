<?php

/**
 * This is the model class for table "issuecatergory_has_knowledge".
 *
 * The followings are the available columns in table 'issuecatergory_has_knowledge':
 * @property integer $issuecatergory_id
 * @property integer $knowledge_id
 */
class IssuecatergoryHasKnowledge extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'issuecatergory_has_knowledge';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('issuecatergory_id, knowledge_id', 'required'),
			array('issuecatergory_id, knowledge_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('issuecatergory_id, knowledge_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'issuecatergory_id' => 'Issuecatergory',
			'knowledge_id' => 'Knowledge',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('issuecatergory_id',$this->issuecatergory_id);
		$criteria->compare('knowledge_id',$this->knowledge_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return IssuecatergoryHasKnowledge the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
