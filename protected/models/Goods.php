<?php 
class Goods extends CActiveRecord{
	public function tableName()
	{
		return 'goods';
	}
	public function rules(){
		return array(
			array('name', 'required'),
			array('active', 'length', 'max'=>2),
			array('annotation', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, annotation, active', 'safe', 'on'=>'search'),
		);
	}
	public function relations(){
		return array(
			'goodsHasGoodsbrands' => array(self::HAS_MANY, 'GoodsHasGoodsbrand', 'goods_id'),
			'issues' => array(self::HAS_MANY, 'Issue', 'goods_id'),
		);
	}
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'ชื่อสินค้า',
			'annotation' => 'หมายเหตุ',
			'active' => 'สถานะสินค้า',
		);
	}
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('annotation',$this->annotation,true);
		$criteria->compare('active',$this->active,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
     public static function getGoodsAll(){
           $mylist= self::model()->model()->findAll(array('select'=>'id, name','condition'=>'active="1"'));
           return CHtml::listData($mylist, 'id', 'name');
       }
       public static function getlebel_Goods($id){ 
          $mylist= self::model()->find(array('select'=>'id, name','condition'=>'active="1"','id'=>$id));
          if($mylist!=NULL){
             return  $mylist->name;
          } 
         return 'ไม่พบข้อมูลสินค้า';
       }
        
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
