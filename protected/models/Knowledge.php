<?php

/**
 * This is the model class for table "knowledge".
 *
 * The followings are the available columns in table 'knowledge':
 * @property integer $id
 * @property string $subject
 * @property string $subjectdetail
 * @property string $active
 * @property integer $users_id
 *
 * The followings are the available model relations:
 * @property Issuecatergory[] $issuecatergories
 * @property Users $users
 */
class Knowledge extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'knowledge';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('subject, subjectdetail, users_id', 'required'),
			array('users_id', 'numerical', 'integerOnly'=>true),
			array('active', 'length', 'max'=>2),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, subject, subjectdetail, active, users_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'issuecatergories' => array(self::MANY_MANY, 'Issuecatergory', 'issuecatergory_has_knowledge(knowledge_id, issuecatergory_id)'),
			'users' => array(self::BELONGS_TO, 'Users', 'users_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'subject' => 'หัวข้อเรื่อง',
			'subjectdetail' => 'รายละเอียด',
			'active' => 'สถานะ',
			'users_id' => 'Users',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('subject',$this->subject,true);
		$criteria->compare('subjectdetail',$this->subjectdetail,true);
		$criteria->compare('active',$this->active,true);
		$criteria->compare('users_id',$this->users_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Knowledge the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
