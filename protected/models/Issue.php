<?php 
class Issue extends CActiveRecord{
	public function tableName()
	{
		return 'issue';
	}
	public function rules()
	{
		return array(
			array('subject, subjectdetail, users_id, goodsmodel_id, goodsbrand_id, goods_id', 'required'),
			array('users_id, goodsmodel_id, goodsbrand_id, goods_id', 'numerical', 'integerOnly'=>true),
			array('manage, annotation, create_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, subject, subjectdetail, manage, annotation, create_at, users_id, goodsmodel_id, goodsbrand_id, goods_id', 'safe', 'on'=>'search'),
		);
	}
	public function relations(){
		return array(
			'goods' => array(self::BELONGS_TO, 'Goods', 'goods_id'),
			'goodsbrand' => array(self::BELONGS_TO, 'Goodsbrand', 'goodsbrand_id'),
			'goodsmodel' => array(self::BELONGS_TO, 'Goodsmodel', 'goodsmodel_id'),
			'users' => array(self::BELONGS_TO, 'Users', 'users_id'),
			'issuecatergories' => array(self::MANY_MANY, 'Issuecatergory', 'issue_has_issuecatergory(issue_id, issuecatergory_id)'),
			'userinforms' => array(self::HAS_MANY, 'Userinform', 'issue_id'),
		);
	}
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'subject' => 'แจ้งเรื่อง',
			'subjectdetail' => 'รายละเอียดการแจ้ง',
			'manage' => 'การดำเนินการ',
			'annotation' => 'หมายเหตุ',
			'create_at' => 'แจ้งเมื่อ',
			'users_id' => 'ผู้บันทึกข้อมูล',
			'goodsmodel_id' => 'รุ่นสินค้า',
			'goodsbrand_id' => 'ยี่ห้อสินค้า',
			'goods_id' => 'สินค้า',
		);
	} 
	public function search(){
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('subject',$this->subject,true);
		$criteria->compare('subjectdetail',$this->subjectdetail,true);
		$criteria->compare('manage',$this->manage,true);
		$criteria->compare('annotation',$this->annotation,true);
		$criteria->compare('create_at',$this->create_at,true);
		$criteria->compare('users_id',$this->users_id);
		$criteria->compare('goodsmodel_id',$this->goodsmodel_id);
		$criteria->compare('goodsbrand_id',$this->goodsbrand_id);
		$criteria->compare('goods_id',$this->goods_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	} 
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
