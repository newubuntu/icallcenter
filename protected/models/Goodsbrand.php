<?php
class Goodsbrand extends CActiveRecord{

	public function tableName()
	{
		return 'goodsbrand';
	} 
	public function rules(){
		return array(
			array('name', 'required'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name', 'safe', 'on'=>'search'),
		);
	}
	public function relations(){
		return array(
			'goodsHasGoodsbrands' => array(self::HAS_MANY, 'GoodsHasGoodsbrand', 'goodsbrand_id'),
			'goodsmodels' => array(self::HAS_MANY, 'Goodsmodel', 'goodsbrand_id'),
			'issues' => array(self::HAS_MANY, 'Issue', 'goodsbrand_id'),
		);
	}
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'ชื่อยี่ห้อ',
		);
	}
 
	public function search(){
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        public static function getGoodsbrandAll(){
           $mylist= self::model()->model()->findAll();
           return CHtml::listData($mylist, 'id', 'name');
       }
       public static function getlebel_Goodsbrand($id){ 
          $mylist= self::model()->find('id=:n',array(':n'=>$id));
          if($mylist!=NULL){
             return  $mylist->name;
          } 
         return 'ไม่พบข้อมูลยี่ห้อสินค้า';
       }
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
