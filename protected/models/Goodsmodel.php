<?php
 
class Goodsmodel extends CActiveRecord{
	public function tableName()
	{
		return 'goodsmodel';
	}
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, goodsbrand_id', 'required'),
			array('goodsbrand_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, goodsbrand_id', 'safe', 'on'=>'search'),
		);
	}
	public function relations(){
		return array(
			'goodsbrand' => array(self::BELONGS_TO, 'Goodsbrand', 'goodsbrand_id'),
			'issues' => array(self::HAS_MANY, 'Issue', 'goodsmodel_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'ชื่อรุ่น',
			'goodsbrand_id' => 'ยี่ห้อ',
		);
	} 
	public function search(){
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('goodsbrand_id',$this->goodsbrand_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
     public static function getGoodsmodelAll(){
           $mylist= self::model()->model()->findAll();
           return CHtml::listData($mylist, 'id', 'name');
       }
       public static function getlebel_Goodsmodel($id){ 
          $mylist= self::model()->find('id=:n',array(':n'=>$id));
          if($mylist!=NULL){
             return  $mylist->name;
          } 
         return 'ไม่พบข้อมูลรุ่นสินค้า';
       }
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
