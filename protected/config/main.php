<?php

Yii::setPathOfAlias('bootstraps', dirname(__FILE__) . '/../extensions/yiibooster');
Yii::setPathOfAlias('adminlte', 'protected/extensions/adminlte');
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'icallcenter',
    'timeZone' => 'Asia/Bangkok',
    'defaultController' => 'Default',
    //'language' => 'th',
    'theme'=>'frontend',
    // preloading 'log' component
    'preload' => array('bootstraps', 'log'),
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
        'application.modules.user.models.*',
        'application.modules.user.components.*',
        'application.modules.rights.*',
        'application.modules.rights.components.*',
    ),
    'aliases' => array(// Yii::log(Yii::getPathOfAlias('users')); // c:/www/mysite\uploads\users\
        'uploads' => 'webroot.uploads', // /www/mysite/uploads OR c:/www/mysite\uploads
        'files' => 'webroot.uploads.files', // /www/mysite/uploads/files OR c:/www/mysite\uploads\files
        'users' => 'webroot.uploads.users', // /www/mysite/uploads/users OR c:/www/mysite\uploads\users
    ),
    'modules' => array(
        'admin', 
        'user' => array(
            'tableUsers' => 'users',
            'tableProfiles' => 'profiles',
            'tableProfileFields' => 'profiles_fields',
            # encrypting method (php hash function)
            'hash' => 'md5',
            # send activation email
            'sendActivationMail' => false, //true
            # allow access for non-activated users
            'loginNotActiv' => false,
            # activate user on registration (only sendActivationMail = false)
            'activeAfterRegister' => true, //false
            # automatically login from registration
            'autoLogin' => true,
            # registration path
            'registrationUrl' => array('/user/registration'),
            # recovery password path
            'recoveryUrl' => array('/user/recovery'),
            # login form path
            'loginUrl' => array('/user/login'),
            # page after login
            'returnUrl' => array('/user/profile'),
            # page after logout
            'returnLogoutUrl' => array('/user/login'),
        ),
        'rights' => array(
            'install' => false,
        ),
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => 'root',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => array('127.0.0.1', '::1'),
            'generatorPaths' => array(
                'bootstraps.gii'
            ),
        ),
    ),
    // application components
    'components' => array(
         'adminlte',
        'user' => array(
            'class' => 'RWebUser',
            'allowAutoLogin' => true,
            'loginUrl' => array('/user/login'),
        ),
        'authManager' => array(
            'class' => 'RDbAuthManager',
            'connectionID' => 'db',
            'itemTable' => 'authitem',
            'itemChildTable' => 'authitemchild',
            'assignmentTable' => 'authassignment',
            'rightsTable' => 'rights',
            'defaultRoles' => array('Guest'),
        ),
        'bootstraps' => array(
            'class' => 'ext.yiibooster.components.Booster',
            //'coreCss' => false,
            'responsiveCss' => true,
        ),
        // uncomment the following to enable URLs in path-format
        /*
          'urlManager'=>array(
          'urlFormat'=>'path',
          'rules'=>array(
          '<controller:\w+>/<id:\d+>'=>'<controller>/view',
          '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
          '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
          ),
          ),
         */
        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=icallcenter',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => 'admin',
            'charset' => 'utf8',
        ),
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),  
        'widgetFactory' => array(
            'widgets' => array(
                'CLinkPager' => array(
                    // 'id'=>'pagination',
                    'internalPageCssClass' => 'dddd',
                    'nextPageLabel' => 'Next',
                    'prevPageLabel' => 'Prev',
                    'selectedPageCssClass' => 'active',
                    'hiddenPageCssClass' => 'disabled',
                    'htmlOptions' => array('class' => 'aaa',)))),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
            // uncomment the following to show log messages on web pages
            /*
              array(
              'class'=>'CWebLogRoute',
              ),
             */
            ),
        ),
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        // this is used in contact page
        'adminEmail' => 'webmaster@example.com',
    ),
);
