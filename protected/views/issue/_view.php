<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('subject')); ?>:</b>
	<?php echo CHtml::encode($data->subject); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('subjectdetail')); ?>:</b>
	<?php echo CHtml::encode($data->subjectdetail); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('manage')); ?>:</b>
	<?php echo CHtml::encode($data->manage); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('annotation')); ?>:</b>
	<?php echo CHtml::encode($data->annotation); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_at')); ?>:</b>
	<?php echo CHtml::encode($data->create_at); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('users_id')); ?>:</b>
	<?php echo CHtml::encode($data->users_id); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('goodsmodel_id')); ?>:</b>
	<?php echo CHtml::encode($data->goodsmodel_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('goodsbrand_id')); ?>:</b>
	<?php echo CHtml::encode($data->goodsbrand_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('goods_id')); ?>:</b>
	<?php echo CHtml::encode($data->goods_id); ?>
	<br />

	*/ ?>

</div>
