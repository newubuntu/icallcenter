<?php
$this->breadcrumbs=array(
	'Issuecatergories'=>array('index'),
	$model->name,
);

$this->menu=array(
array('label'=>'List Issuecatergory','url'=>array('index')),
array('label'=>'Create Issuecatergory','url'=>array('create')),
array('label'=>'Update Issuecatergory','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Issuecatergory','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Issuecatergory','url'=>array('admin')),
);
?>

<h1>View Issuecatergory #<?php echo $model->id; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'name',
		'active',
),
)); ?>
