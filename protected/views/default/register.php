<div class="form-box" id="login-box">
    <div class="header">Register New Member</div>
    <div class="form">
	<?php if(Yii::app()->user->hasFlash('register')) : ?>

<div class="flash-success" align="center">
<div class="confirmation-box round" style="width:600px; text-align:left">
	<?php echo Yii::app()->user->getFlash('register'); ?>
</div>
</div>

<?php endif; ?>

<?php if(Yii::app()->user->hasFlash('registererror')) : ?>

<div class="flash-error" align="center">
<div class="error-box round" style="width:600px; text-align:left">
	<?php echo Yii::app()->user->getFlash('registererror'); ?>
</div>
</div>

<?php endif; ?>
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'login-form',
            'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
            ),
        ));
        ?>
        <div class="body bg-gray">
            <p class="note">Fields with <span class="required">*</span> are required.</p>
            <div class="form-group">
                <?php echo $form->textField($model, 'username', array('class' => 'form-control', 'value' => '', 'placeholder' => $model->getAttributeLabel('username'))); ?>
                <?php echo $form->error($model, 'username'); ?> 
            </div>
            <div class="form-group">
                <?php echo $form->passwordField($model, 'password', array('class' => 'form-control', 'value' => '', 'placeholder' => $model->getAttributeLabel('password'))); ?>
                <?php echo $form->error($model, 'password'); ?>
            </div>          
            <div class="form-group">
                 <?php echo $form->textField($model, 'firstname', array('class' => 'form-control', 'value' => '', 'placeholder' => $model->getAttributeLabel('firstname'))); ?>
                <?php echo $form->error($model, 'firstname'); ?>
            </div>
            <div class="form-group">
                 <?php echo $form->textField($model, 'lastname', array('class' => 'form-control', 'value' => '', 'placeholder' => $model->getAttributeLabel('lastname'))); ?>
                <?php echo $form->error($model, 'lastname'); ?>
            </div>
        </div>
        <div class="footer">                                                               
            <?php echo CHtml::submitButton('Register', array('class' => 'btn bg-olive btn-block')); ?>
        <!--    <p><a href="#">I forgot my password</a></p>
            <a href="#" class="text-center">Register a new membership</a> -->
			<?=CHtml::link('I already have a member',array('default/login'),array('class'=>'text-center','style'=>'padding-left:105px;'));?>
        </div>
        <?php $this->endWidget(); ?>
    </div><!-- form -->

</div>
