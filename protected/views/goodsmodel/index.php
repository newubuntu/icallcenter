<?php
$this->breadcrumbs=array(
	'Goodsmodels',
);

$this->menu=array(
array('label'=>'Create Goodsmodel','url'=>array('create')),
array('label'=>'Manage Goodsmodel','url'=>array('admin')),
);
?>

<h1>Goodsmodels</h1>

<?php $this->widget('booster.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
