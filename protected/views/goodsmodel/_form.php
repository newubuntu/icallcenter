<?php
$form = $this->beginWidget('booster.widgets.TbActiveForm', array(
    'id' => 'goodsmodel-form',
    'enableAjaxValidation' => false,
    'type' => 'horizontal',
    'htmlOptions' => array('class' => 'well',
        'enctype' => 'multipart/form-data')
        ));
?>
<?php
$mytrue = TRUE;
if (!$model->isNewRecord) {
    $mytrue = FALSE;
}
?>

<fieldset class="myfieldset">
    <?php
    if ($mytrue) {
        ?>
        <legend class="mylegend"><i class="glyphicon glyphicon-plus"></i>เพิ่มรุ่นสินค้า</legend>
        <?php
    } else {
        ?>
        <legend class="mylegend"><i class="glyphicon glyphicon-pencil"></i>แก้ไขรุ่นสินค้า&nbsp;<?= $model->name ?></legend>    
        <?php
    }
    ?>
    <div style="margin-left:270px;">
        <p class="help-block">โปรดกรอกข้อมูลให้ครบทุกช่อง<span class="required">    *</span> </p>
    </div>
    <?php echo $form->errorSummary($model); ?>
    <?php echo $form->textFieldGroup($model, 'name', array('widgetOptions' => array('htmlOptions' => array('style' => 'width:450px;')))); ?> 
    <?php echo $form->dropDownListGroup($model, 'goodsbrand_id', array('wrapperHtmlOptions' => array('class' => 'col-sm-5'), 'widgetOptions' => array('data' => Goodsbrand::getGoodsbrandAll(), 'htmlOptions' => array('prompt' => 'เลือกยี่ห้อสินค้า', 'style' => 'width:250px;')))); ?>
    <div class="form-actions" style="margin-left:280px;">
        <?php
        $this->widget('booster.widgets.TbButton', array(
            'buttonType' => 'submit',
            'context' => 'primary',
            'label' => $model->isNewRecord ? 'บันทึก' : 'แก้ไข',
        ));
        ?>
        <?php
        $this->widget('booster.widgets.TbButton', array(
            'buttonType' => 'reset', 'label' => 'ยกเลิก')
        );
        ?>
    </div>
</fieldset>
<?php $this->endWidget(); ?>
