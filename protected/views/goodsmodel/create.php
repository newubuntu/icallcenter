<?php
$this->breadcrumbs=array(
	'Goodsmodels'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List Goodsmodel','url'=>array('index')),
array('label'=>'Manage Goodsmodel','url'=>array('admin')),
);
?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>