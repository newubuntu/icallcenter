<?php
$this->breadcrumbs=array(
	'Goodsbrands'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List Goodsbrand','url'=>array('index')),
array('label'=>'Manage Goodsbrand','url'=>array('admin')),
);
?> 

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>