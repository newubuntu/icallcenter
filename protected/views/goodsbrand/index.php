<?php
$this->breadcrumbs=array(
	'Goodsbrands',
);

$this->menu=array(
array('label'=>'Create Goodsbrand','url'=>array('create')),
array('label'=>'Manage Goodsbrand','url'=>array('admin')),
);
?>

<h1>Goodsbrands</h1>

<?php $this->widget('booster.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
