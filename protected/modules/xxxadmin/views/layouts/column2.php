<?php $this->beginContent('/layouts/main'); ?>
<div class="row">
    <div class="col-xs-2" id="sidebar">
        <?php
        if (isset($this->clips['adminMenuClip']))
            echo $this->clips['adminMenuClip'];
        ?>
    </div>
    <div class="col-xs-10" id="main-content">
        <?php echo $content; ?>
    </div>
</div>
<?php $this->endContent(); ?>