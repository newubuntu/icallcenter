<?php
class AdminModule extends CWebModule{
    public $theme = 'default';
    /**
     * The minimum Bootstrap version, configured in main config, compatible with this module
     */
    const MIN_BOOTSTRAP_VERSION = '2.0.2';

    /**
     * @var string the path to this modules published asset directory
     */
    protected $assetsUrl;

    /**
     * @var boolean indicates whether assets should be republished on every request.
     */
    public $forceCopyAssets = false;

    /**
     * @var boolean indicates if the Bootstrap extension is installed and configured in the main config.
     */
    protected $isBootstrapInstalled = false;

    /**
     * @var string the bootstrap version installed and configured in the main config
     */
    protected $bootstrapVersion = null;

    public function preinit() {
        $this->checkDependencies();

        Yii::setPathOfAlias('Booster', 'protected/modules/admin/library/bootstrap');
        Yii::setPathOfAlias('adminlte','protected/extensions/adminlte');
       
        // Reset the front-end's client script because we don't want
        // both front-end styles being applied in this module.
        Yii::app()->clientScript->reset();
    }
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		$this->setLayoutPath('protected/modules/admin/views/layouts');
        $this->layout = 'main';
        Yii::app()->name = Yii::app()->name.' - Admin Panel';
		// import the module-level models and components
		$this->setImport(array(
			'admin.models.*',
			'admin.components.*',
			'rights.components.*',
			'rights.components.behaviors.*',
			'rights.components.dataproviders.*',
			'rights.controllers.*',
			'rights.models.*',
			'admin.library.*',
			//'application.components.ActiveRecords.*'
		));
                
                $this->setComponents(array(
					 'components' => array(
                    'Booster' => array(
                    'class' => 'AdminBootstrap',
                    'forceCopyAssets' => $this->forceCopyAssets
                   ),'adminlte' 
                  ),
                    'errorHandler' => array(
                        'errorAction' => 'admin/default/error'),
                        'user' => array(
                        'allowAutoLogin'=>true,
						'class' => 'CWebUser',             
                        'loginUrl' => Yii::app()->createUrl('/admin/default/login'),
                    )
                ));
                
                Yii::app()->theme = 'admin/' . $this->theme;
                //Yii::app()->params['defaultPageSize'] = 10;

		// Set theme url
                Yii::app()->themeManager->setBaseUrl( Yii::app()->theme->baseUrl );
                Yii::app()->themeManager->setBasePath( Yii::app()->theme->basePath );
                Yii::app()->user->setStateKeyPrefix('_admin');
		 $this->registerCoreCss(); 
         $this->registerBootstrap();
         $this->registerjs();
	}

	public function beforeControllerAction($controller, $action){
			if(parent::beforeControllerAction($controller, $action))
            {
				
                $controller->layout="main";
				Yii::app()->widgetFactory->widgets['CBreadcrumbs']=array( 'homeLink'=>CHtml::link('Home', array('/admin')));
                // this method is called before any module controller action is performed
                // you may place customized code here
                 $route = $controller->id . '/' . $action->id;
                 $publicPages = array(
                    'default/login',
                    'default/error',
					'default/register',
                 );
                if (Yii::app()->user->isGuest && !in_array($route, $publicPages))
                {
                    Yii::app()->getModule('admin')->user->setReturnUrl('index');      
             
                    Yii::app()->getModule('admin')->user->loginRequired();                
                }
				else
                {
                    Yii::app()->getModule('admin')->user->setReturnUrl('index');  
                    return true;
                }
            }
            else
                    return false;
	}
    /**
     * Initializes the Bootstrap component
     */
    protected function registerBootstrap(){ 
		//var_dump( $this->getComponent('Booster')->registerPackage('Booster'));exit();
        $this->getComponent('Booster');
    }

    /**
     * Registers the published admin CSS
     */
    protected function registerCoreCss(){
        Yii::app()->clientScript->registerCssFile($this->getAssetsUrl() . '/css/font-awesome.min.css');
		Yii::app()->clientScript->registerCssFile($this->getAssetsUrl() . '/css/bootstrap.min.css');
        Yii::app()->clientScript->registerCssFile($this->getAssetsUrl() . '/css/AdminLTE.css');
    }
    protected function registerjs(){ 
     Yii::app()->getClientScript()->registerScriptFile($this->getAssetsUrl() . '/js/AdminLTE/app.js');
    }
    /**
     * Publishes and returns the URL to the assets folder.
     * @return string the URL
     */
    public function getAssetsUrl()
    {
        if (!isset($this->assetsUrl))
        {
            $assetsPath = Yii::getPathOfAlias('admin.assets');
            $this->assetsUrl = Yii::app()->assetManager->publish($assetsPath, false, -1, $this->forceCopyAssets);
        }

        return $this->assetsUrl;
    }
    /**
     * Checks if the Bootstrap extension is installed and verifies it is capable
     */
    protected function checkDependencies(){
        $this->isBootstrapInstalled = Yii::app()->hasComponent('Booster');
        if ($this->isBootstrapInstalled)
        {
            $this->bootstrapVersion = Yii::app()->bootstrap->getVersion();

            if ($this->bootstrapVersion < self::MIN_BOOTSTRAP_VERSION)
                throw new Exception("Please update your Bootstrap extension to at least " . self::MIN_BOOTSTRAP_VERSION);
        }
    }
}
