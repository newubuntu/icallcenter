<?php

class Mail extends CComponent
{
	public $mail = null;

	public function __construct()
	{
		$this->mail = Yii::app()->mailer;
		$this->mail->setLanguage('ru');
//		$settings = SettingsMail::model()->find();

        $settings = Yii::app()->params['smtp'] ;

		if(isset($settings['smtp_host']))
		{
			$this->mail->isSMTP();
//			$this->mail->SMTPAuth = true;

			$this->mail->Host = $settings['smtp_host'];
			$this->mail->Username = $settings['smtp_username'];
			$this->mail->Password = $settings['smtp_password'];

            if ($settings['tls'])
			    $this->mail->SMTPSecure = 'tls';

			$this->mail->Port = $settings['smtp_port'];
		}
		else
		{
			$this->mail->IsSendmail();
//			if(!empty($settings->sendmail_path))
//				$this->mail->Sendmail = $settings->sendmail_path;
		}

		$this->mail->SetFrom($settings['smtp_username'], Yii::app()->name);
		$this->mail->AddReplyTo($settings['smtp_username'],Yii::app()->name);
		$this->mail->isHTML(true);

	}

    public function adminNotify($subject, $body) {
        $result = $this->send(Yii::app()->params['adminEmail'], $subject, $body);

        if (!$result) {


        }

        return $result;

    }

    public function send($email, $subject, $body)
    {
		$this->mail->addAddress($email);
		$this->mail->Subject = $subject;
		$this->mail->Body    = $body;
        return $this->mail->send();
    }
}
