<?php

class BackController extends AdminController
{
    public function actionIndex()
    {
        $model = SettingsMail::model()->find();

        if (isset($_POST['SettingsMail']))
        {
            $model->attributes = $_POST['SettingsMail'];

            if ($model->save())
                $this->redirect(array('index'));
        }

        $this->render('index', array(
            'model' => $model,
        ));
    }
}