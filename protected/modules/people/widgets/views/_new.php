						<div class="container">
							<div class="image">
								<?php echo CHtml::image('/uploads/people/'.$data->avatar, $data->avatar); ?>
							</div>
							<div class="title"><?php echo CHtml::encode($data->last_name);?> <?php echo CHtml::encode($data->middle_name); ?> <?php echo CHtml::encode($data->first_name);?></div>
						</div>