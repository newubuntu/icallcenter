<?php
Yii::import('application.modules.people.models.People');
/*
 *Класс виджета для вывода товаров на главную страницу
 *
*/
class PeopleYearWidget extends CWidget {
	public $year;
	public function	run() {
		$this->year = ($this->year) ? (int) $this->year : false;
        // устанавливаем условие для отбора
        $criteria=new CDbCriteria;
       
		$criteria->select='id,first_name,last_name,middle_name,avatar,birthday,deathdate';
        $criteria->limit=3;
        $criteria->order='birthday DESC';
        if($this->year) $criteria->condition = "YEAR(`birthday`) = $this->year";
        $dataProvider = new CActiveDataProvider('People', array(
            'criteria'=>$criteria,
            'pagination'=>false,
        ));

		$this->render('yearPeople',array(
                           'dataProvider'=>$dataProvider,
        ));
        
		return parent::run();
        
	}

}
?>
