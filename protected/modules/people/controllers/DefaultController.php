<?php

class DefaultController extends RController

{

	public $layout='//layouts/column2';
	
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	public function actionCreate()
	{
		$model=new People;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['People']))
		{
			$model->attributes=$_POST['People'];
			if ($model->validate()) 
			{			
				$file = CUploadedFile::getInstance($model, 'avatar');
				
					if($file != null && $file != ""){
							$dir=$_SERVER['DOCUMENT_ROOT'].'/uploads/people/';
							$name = uniqid().'.'.$file->getExtensionName();
            	Yii::app()->ih->load($file->tempName)->resize(220, 165, true)->save($dir.$name);
							$model->avatar = $name;
					}		
            	
			}
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}


	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$modelPre = $model;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['People']))
		{
			$model->attributes=$_POST['People'];
			if ($model->validate()) 
			{
				$file = CUploadedFile::getInstance($model, 'avatar');
				if ($file!="") 
				{
					$dir=$_SERVER['DOCUMENT_ROOT'].'/uploads/people/';
					unlink($dir.$modelPre->avatar);
					$name = uniqid().'.'.$file->getExtensionName();
					Yii::app()->ih->load($file->tempName)->resize(220, 165, true)->save($dir.$name);
            		$model->avatar = $name;
      
            	}
			}
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}
	
	
	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
		$model->delete();
		unlink($_SERVER['DOCUMENT_ROOT'].'/uploads/people/'.$model->avatar);
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
	
	
	public function actionIndex(){
		$dataProvider=new CActiveDataProvider('People');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}
	
	public function actionAdmin()
	{
		$model=new People('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['People']))
			$model->attributes=$_GET['People'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
	
	public function loadModel($id)
	{
		$model=People::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='people-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

}
