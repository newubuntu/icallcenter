<?php

/**
 * This is the model class for table "People".
 *
 * The followings are the available columns in table 'People':
 * @property integer $id
 * @property string $last_name
 * @property string $first_name
 * @property string $middle_name
 * @property string $birthday
 * @property string $deathdate
 * @property string $biography
 * @property string $avatar
 * @property integer $public
 * @property integer $deleted
 */
class People extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return People the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'People';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('avatar', 'file', 'types'=>'jpg,jpeg,gif,png','allowEmpty'=>true),
			array('last_name, first_name, middle_name, birthday,, biography', 'required'),
			array( 'deathdate', 'safe'),
			array('public, deleted', 'numerical', 'integerOnly'=>true),
			array('last_name, first_name, middle_name, avatar', 'length', 'max'=>256),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, last_name, first_name, middle_name, birthday, deathdate, biography, avatar, public, deleted', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'last_name' => 'Фамилия',
			'first_name' => 'Имя',
			'middle_name' => 'Отчество',
			'birthday' => 'Дата рождения',
			'deathdate' => 'Дата смерти',
			'biography' => 'Биография',
			'avatar' => 'Фото',
			'public' => 'Видимый',
			'deleted' => 'Удаленный',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('middle_name',$this->middle_name,true);
		$criteria->compare('birthday',$this->birthday,true);
		$criteria->compare('deathdate',$this->deathdate,true);
		$criteria->compare('biography',$this->biography,true);
		$criteria->compare('avatar',$this->avatar,true);
		$criteria->compare('public',$this->public);
		$criteria->compare('deleted',$this->deleted);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}