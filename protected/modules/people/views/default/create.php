<?php
/* @var $this PeopleController */
/* @var $model People */

$this->breadcrumbs=array(
	'Личности'=>array('index'),
	'Добавление личности',
);

$this->menu=array(
	array('label'=>'Список личностей', 'url'=>array('index')),
	array('label'=>'Управление личностями', 'url'=>array('admin')),
);
?>

<h1>Добавление личности</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>