<?php
/* @var $this PeopleController */
/* @var $model People */

$this->breadcrumbs=array(
	'Личности'=>array('index'),
	$model->first_name.' '.$model->middle_name.' '.$model->last_name,
);

$this->menu=array(
	array('label'=>'Список личностей', 'url'=>array('index')),
	array('label'=>'Добавить личность', 'url'=>array('create')),
	array('label'=>'Изменить личность', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Удалить личность', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Удалить личность?')),
	array('label'=>'Управление личностями', 'url'=>array('admin')),
);
?>
<br>
						<div class="article">
							<div class="date btn">
								<?php echo Yii::app()->dateFormatter->format("dd MMMM yyyy",$model->birthday);?>
								<?php if (CHtml::encode($model->deathdate)!="0000-00-00")  
									echo " - ".Yii::app()->dateFormatter->format("dd MMMM yyyy",$model->deathdate);
								?>
							</div>
							<div class="title"><?php echo CHtml::encode($model->first_name);?> <?php echo CHtml::encode($model->middle_name);?> <?php echo CHtml::encode($model->last_name);?></div>
							<div class="text">
								<?if($model->avatar != "" && file_exists($_SERVER['DOCUMENT_ROOT']."/uploads/people/".$model->avatar)):?>
									<div class="image">
										<?php echo CHtml::image('/uploads/people/'.$model->avatar, $model->avatar);?>
									</div>
								<?endif?>
								<?php echo CHtml::encode($model->biography); ?>
							</div>
						</div>
