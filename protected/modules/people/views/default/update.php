<?php
/* @var $this PeopleController */
/* @var $model People */

$this->breadcrumbs=array(
	'Личности'=>array('index'),
	$model->last_name.' '.$model->first_name.' '.$model->middle_name=>array('view','id'=>$model->id),
	'Изменение',
);

$this->menu=array(
	array('label'=>'Список личностей', 'url'=>array('index')),
	array('label'=>'Добавить личность', 'url'=>array('create')),
	array('label'=>'Просмотр личности', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Управление личностями', 'url'=>array('admin')),
);
?>

<h1>Изменение информации о  <?php echo $model->last_name.' '.$model->first_name.' '.$model->middle_name; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>