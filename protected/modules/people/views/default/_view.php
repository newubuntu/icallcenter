	<div class="article">
	    <div class="date btn">
	    	<?php echo Yii::app()->dateFormatter->format("dd MMMM yyyy",$data->birthday);?>
	    	<?php if (CHtml::encode($data->deathdate)!="0000-00-00") 
	    		echo "- ".Yii::app()->dateFormatter->format("dd MMMM yyyy",$data->deathdate);
	    	?>
	    </div>
	    <div class="title"><?php echo CHtml::encode($data->last_name);?> <?php echo CHtml::encode($data->first_name);?> <?php echo CHtml::encode($data->middle_name);?></div>
	    <div class="text">
	    	<?if($data->avatar != "" && file_exists($_SERVER['DOCUMENT_ROOT']."/uploads/people/".$data->avatar)):?>
	    		<div class="image">
	    			<?php echo CHtml::image('/uploads/people/'.$data->avatar, $data->avatar);?>
	    		</div>
	    	<?endif?>
	    	<?php echo substr(CHtml::encode($data->biography), 0, 250); ?>
	    	<a href="/people/default/view/id/<?php echo CHtml::encode($data->id);?>" class="more">Читать далее »</a>
	    </div>
	</div>
