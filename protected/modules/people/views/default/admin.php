<?php
/* @var $this PeopleController */
/* @var $model People */

$this->breadcrumbs=array(
	'Личности'=>array('index'),
	'Управление личностями',
);

$this->menu=array(
	array('label'=>'Список личностей', 'url'=>array('index')),
	array('label'=>'Добавить личность', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#people-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Управление личностями</h1>


<?php echo CHtml::link('Расширенный поиск','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'people-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'last_name',
		'first_name',
		'middle_name',
		'birthday',
		'deathdate',
		/*
		'biography',
		*/
		'avatar',
		'public',
		'deleted',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
