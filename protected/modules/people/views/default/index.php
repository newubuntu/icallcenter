<?php
/* @var $this PeopleController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Личности',
);
if (Yii::app()->user->checkAccess('Admin') or Yii::app()->user->checkAccess('Moderator') or Yii::app()->user->checkAccess('Editor'))
$this->menu=array(
	array('label'=>'Добавить личность', 'url'=>array('create')),
	array('label'=>'Управление личностями', 'url'=>array('admin')),
);
?>


<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
