<?php

class IssueController extends MYAdminController {
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }
    public function actionCreate() {
        $model = new Issue;
        if (isset($_POST['Issue'])) {
            $model->attributes = $_POST['Issue'];
            $model->users_id=Yii::app()->user->id;
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionUpdate($id) {
        $model = $this->loadModel($id);

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

        if (isset($_POST['Issue'])) {
            $model->attributes = $_POST['Issue'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
// we only allow deletion via POST request
            $this->loadModel($id)->delete();

// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Issue');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    public function actionAdmin() {
        $model = new Issue('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Issue']))
            $model->attributes = $_GET['Issue'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionAjaxAdmin() {
        error_reporting(0);
        // SQL limit
        $sLimit = '';
        if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
            $sLimit = 'LIMIT ' . (int) $_GET['iDisplayStart'] . ', ' . (int) $_GET['iDisplayLength'];
        }

        // SQL order
        $aColumns = array('id', 'subject', 'subjectdetail', 'manage', 'annotation');
        $sOrder = '';
        if (isset($_GET['iSortCol_0'])) {
            $sOrder = 'ORDER BY  ';
            for ($i = 0; $i < (int) $_GET['iSortingCols']; $i++) {
                if ($_GET['bSortable_' . (int) $_GET['iSortCol_' . $i]] == 'true') {
                    $sOrder .= '`' . $aColumns[(int) $_GET['iSortCol_' . $i]] . '` ' .
                            ($_GET['sSortDir_' . $i] === 'asc' ? 'asc' : 'desc') . ', ';
                }
            }

            $sOrder = substr_replace($sOrder, '', -2);
            if ($sOrder == 'ORDER BY') {
                $sOrder = '';
            }
        }

        // SQL where
        $sWhere = 'WHERE 1';
        if (isset($_GET['sSearch']) && $_GET['sSearch'] != '') {
            $sWhere = 'WHERE 1 AND (';
            for ($i = 0; $i < count($aColumns); $i++) {
                if (isset($_GET['bSearchable_' . $i]) && $_GET['bSearchable_' . $i] == 'true') {
                    $sWhere .= '`' . $aColumns[$i] . "` LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' OR ";
                }
            }
            $sWhere = substr_replace($sWhere, '', -3);
            $sWhere .= ')';
        }
        $sql1 = "SELECT * FROM issue {$sWhere} {$sOrder} {$sLimit}";
        $sql2 = "SELECT COUNT('id') AS Cnt FROM issue WHERE 1";
         // print_r($_GET);exit();
        $dbCommand = Yii::app()->db->createCommand($sql1);
        $aMembers = $dbCommand->queryAll();
        $dbCommand = Yii::app()->db->createCommand($sql2);
        $iCnt = $dbCommand->queryScalar();
        $output = array(
            'sEcho' => intval($_GET['sEcho']),
            'iTotalRecords' => count($aMembers),
            'iTotalDisplayRecords' => $iCnt,
            'aaData' => array()
        );
        $myi = 0;
        $myi = $_GET['iDisplayStart'];
        if ($myi <= 0) {
            $myi = 0;
        }

        foreach ($aMembers as $iID => $aInfo) {
            $myi++;
            $aItem = array(
                $myi,
                $aInfo['id'],
                $aInfo['subject'],
                $aInfo['subjectdetail'],
                $aInfo['manage'],
                $aInfo['annotation'],
                'DT_RowId' => $aInfo['id']
            );
            $output['aaData'][] = $aItem;
        }
        echo json_encode($output);
    }

    public function loadModel($id) {
        $model = Issue::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'issue-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
