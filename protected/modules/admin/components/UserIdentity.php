<?php
//Yii::import("application.*");
class UserIdentity extends CUserIdentity{
	private $_id;
	const ERROR_EMAIL_INVALID=3;
	const ERROR_STATUS_NOTACTIV=4;
	const ERROR_STATUS_BAN=5;
	public function authenticate()
	{
		if (strpos($this->username, "@")) {
			$user = Users::model()->findByAttributes(array(
				'email' => $this->username
			));
		} else {
			$user = Users::model()->findByAttributes(array(
				'username' => $this->username
			));
		}
		if ($user === null) {
			if (strpos($this->username, "@")) {
				$this->errorCode = self::ERROR_EMAIL_INVALID;
			} else {
				$this->errorCode = self::ERROR_USERNAME_INVALID;
			}
		} elseif (Yii::app()->getModule('user')->encrypting($this->password)!==$user->password) {
			$this->errorCode = self::ERROR_PASSWORD_INVALID;
		} elseif ($user->status==0&&Yii::app()->getModule('user')->loginNotActiv==false) {
			$this->errorCode = self::ERROR_STATUS_NOTACTIV;
		} elseif ($user->status==-1) {
			$this->errorCode = self::ERROR_STATUS_BAN;
		} else {
			$this->_id = $user->id;
			$this->setState('modeluser',$user);
			$this->setState('adminfullname',$user->username);
			$this->errorCode = self::ERROR_NONE;
		}
		return !$this->errorCode;
	}

	public function getId()
	{
		return $this->_id;
	}
}
