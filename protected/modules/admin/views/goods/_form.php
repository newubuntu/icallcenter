<?php
$form = $this->beginWidget('booster.widgets.TbActiveForm', array(
    'id' => 'goods-form',
    'enableAjaxValidation' => false,
    'type' => 'horizontal',
    'htmlOptions' => array('class' => 'well',
        'enctype' => 'multipart/form-data')
        ));
?>
<?php
$mytrue = TRUE;
if (!$model->isNewRecord) {
    $mytrue = FALSE;
}
?>

<fieldset class="myfieldset">
    <?php
    if ($mytrue) {
        ?>
        <legend class="mylegend"><i class="glyphicon glyphicon-plus"></i>เพิ่มสินค้า</legend>
        <?php
    } else {
        ?>
        <legend class="mylegend"><i class="glyphicon glyphicon-pencil"></i>แก้ไขสินค้า&nbsp;<?= $model->name ?></legend>    
        <?php
    }
    ?>
    <div style="margin-left:270px;">
        <p class="help-block">โปรดกรอกข้อมูลให้ครบทุกช่อง<span class="required">    *</span> </p>
    </div>
    <?php echo $form->errorSummary($model); ?>
    <?php echo $form->textFieldGroup($model, 'name', array('widgetOptions' => array('htmlOptions' => array('style' => 'width:450px;')))); ?>
    <?php echo $form->textAreaGroup($model, 'annotation', array('widgetOptions' => array('htmlOptions' => array('rows' => 3, 'style' => 'width:450px;')))); ?>
    <?php echo $form->radioButtonListGroup($model, 'active', array('widgetOptions' => array('data' => array('ไม่แสดงผล', 'แสดงผล')))); ?>

    <div class="form-actions" style="margin-left:280px;">
        <?php
        $this->widget('booster.widgets.TbButton', array(
            'buttonType' => 'submit',
            'context' => 'primary',
            'label' => $model->isNewRecord ? 'บันทึก' : 'แก้ไข',
        ));
        ?>
        <?php
        $this->widget('booster.widgets.TbButton', array(
            'buttonType' => 'reset', 'label' => 'ยกเลิก')
        );
        ?>
    </div>
</fieldset>
<?php $this->endWidget(); ?>
 
