<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- Ionicons -->
        <link rel="shortcut icon" href="<?php echo Yii::app()->baseUrl; ?>/images/uploads/favicon.png" />
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <title><?php echo CHtml::encode($this->pageTitle); ?> </title>
        <script type="text/javascript">

            Array.prototype.remove = function (from, to) {
                var rest = this.slice((to || from) + 1 || this.length);
                this.length = from < 0 ? this.length + from : from;
                return this.push.apply(this, rest);
            };
            Array.prototype.myremove = function (value) {
                var idx = this.indexOf(value);
                if (idx != -1) {
                    return this.splice(idx, 1); // The second parameter is the number of elements to remove.
                }
                return false;
            }
            Array.prototype.arrgetlast = Array.prototype.arrgetlast || function (count) {
                count = count || 1;
                var length = this.length;
                if (count <= length) {
                    return this[length - count];
                } else {
                    return null;
                }
            };
            Array.prototype.myUnique = function () {
                var u = {}, a = [];
                var uk = false;
                for (var i = 0, l = this.length; i < l; ++i) {
                    if (u.hasOwnProperty(this[i])) {
                        uk = true;
                    }
                    a.push(this[i]);
                    u[this[i]] = 1;
                }
                return uk;
            }
        </script>
    </head> 
    <body class="skin-blue"> 
        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="<?= Yii::app()->getHomeUrl(); ?>" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
              ส่วนพนักงาน
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right"> 
                    <?php
                    // var_dump(Yii::app()->user->checkAccess('admin'));
                    //var_dump(Yii::app()->session['user']);
                    $this->widget('zii.widgets.CMenu', array(
                        'htmlOptions' => array('class' => 'nav navbar-nav'),
                        'encodeLabel' => FALSE,
                        'items' => array(
                            //  array('label' => 'Home', 'url' => array('admin/mainadmin')),
                            array('label' => 'เข้าสู่ระบบ', 'url' => array('default/login'), 'visible' => Yii::app()->user->isGuest), //Yii::app()->user->isGuest
                            array('label' => 'ออกจากระบบ', 'url' => array('default/logout'), 'visible' => !Yii::app()->user->isGuest)//Yii::app ()->user->isAdmin ()
                        )
                    ));
                    ?>

                </div>
            </nav>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">                
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <?php
                    $this->widget('adminlte.SidebarMenu', array(
                        'items' => array(
                            //   array('label' => 'Home', 'icon' => 'home', 'url' => array('/admin/mainadmin')),
                            // array('label' => '?????????????', 'url' => 'employee/admin', 'icon' => 'fa fa-th', 'bage' => '<small class="badge pull-right bg-yellow">12</small>'),
                            array('label' => 'จัดการข้อมูลปัญหา', 'url' => '#', 'icon' => 'fa fa-laptop', 'items' => array(
                                    array('label' => 'รายการปัญหา', 'url' => array('issue/admin'), 'visible' => true),
                                    array('label' => 'รายการหมวดปัญหา', 'url' => array('issuecatergory/admin'), 'visible' => true),
                                    array('label' => 'เพิ่มข้อมูลปัญหา', 'url' => array('issue/create'), 'visible' => true),
                                    array('label' => 'เพิ่มข้อมูลหมวดปัญหา', 'url' => array('issuecatergory/create'), 'visible' => true), 
                                )), //,'visible' =>Yii::app ()->user->isAdmin()
                               array('label' => 'จัดการข้อมูลสินค้า', 'url' => '#', 'icon' => 'fa fa-laptop', 'items' => array(
                                    array('label' => 'รายการชื่อสินค้า', 'url' => array('goods/admin'), 'visible' => true),
                                    array('label' => 'รายการยี่ห้อสินค้า', 'url' => array('goodsbrand/admin'), 'visible' => true),
                                    array('label' => 'รายการรุ่นสินค้า', 'url' => array('goodsmodel/admin'), 'visible' => true),
                                    array('label' => 'เพิ่มข้อมูลชื่อสินค้า', 'url' => array('goods/create'), 'visible' => true),
                                    array('label' => 'เพิ่มข้อมูลยี่ห้อสินค้า', 'url' => array('goodsbrand/create'), 'visible' => true),
                                    array('label' => 'เพิ่มข้อมูลรุ่นสินค้า', 'url' => array('goodsmodel/create'), 'visible' => true),
                                )), //,'visible' =>Yii::app ()->user->isAdmin()
                               array('label' => 'จัดการข้อมูลผู้ใช้งาน', 'url' => '#', 'icon' => 'fa fa-laptop', 'items' => array(
                                    array('label' => 'รายการชื่อผู้ใช้งาน', 'url' => array('admin/course/admin'), 'visible' => true),
                                    array('label' => 'เพิ่มข้อมูลผู้ใช้งาน', 'url' => array('admin/course/create'), 'visible' => true), 
                                )), //,'visible' =>Yii::app ()->user->isAdmin()
                         //   array('label' => 'พนักงาน', 'icon' => 'fa fa-laptop', 'items', 'url' => array('Leader/employee/admin'), 'visible' => Yii::app()->user->isLEADER()),

                            /* array('label' => 'บริษัท', 'url' => '#', 'icon' => 'fa fa-laptop', 'items' => array(
                              array('label' => 'จัดข้อมูลบริษัท', 'url' => array('admin/company/admin')),
                              array('label' => 'เพิ่มบริษัท', 'url' => array('admin/company/create')),
                              ),'visible' =>Yii::app ()->user->isAdmin()), */
                            array('label' => 'รายงานระบบ', 'url' => '#', 'icon' => 'fa fa-laptop', 'items' => array(
                                    ///  array('label' => 'ข้อมูลการอณุมัติหลักสูตร', 'url' => array('#'),'visible' =>Yii::app ()->user->isLEADER()),
                                    //array('label' => 'รายชื่อผู้อบรม', 'url' => array('admin/employee/Exportpdf'), 'visible' => Yii::app()->user->isLEADER()),
                                    //array('label' => 'ประวัติ หลักสูตร', 'url' => array('admin/trainingCompleted'), 'visible' => Yii::app()->user->isAdmin()),
                                    //array('label' => 'รายชื่อผู้เข้าอบรม', 'url' => array('admin/result_registerlist'), 'visible' => Yii::app()->user->isAdmin()),
                                    //array('label' => 'สรุปหลักสูตรรายเดือน', 'url' => array('admin/chartreport'), 'visible' => Yii::app()->user->isAdmin()),
                                )),
                        //    array('label' => 'Settings', 'url' => '#', 'icon' => 'fa fa-wrench'),
                        //  array('label' => 'Help', 'url' => '#', 'icon' => 'fa fa-file-text'),
                        )
                            )
                    );
                    ?>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">                
                <!-- Content Header (Page header) -->
                <!-- Main content -->
                <?php Msg::show(); ?>
                <section class="content">
                    <?php echo $content; ?> 
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->
        <!-- jQuery 2.0.2 -->
      <!--  <script src='/js/jquery.min.js' type='text/javascript'></script> -->
        <!-- Bootstrap -->
       <!-- <script src='/js/bootstrap.min.js' type='text/javascript'></script>-->
        <!-- AdminLTE App -->
       <!-- <script src='/js/AdminLTE/app.js' type='text/javascript'></script> -->

    </body>
</html>