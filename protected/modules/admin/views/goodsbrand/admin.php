<?php
$this->breadcrumbs=array(
	'Goodsbrands'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List Goodsbrand','url'=>array('index')),
array('label'=>'Create Goodsbrand','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('goodsbrand-grid', {
data: $(this).serialize()
});
return false;
});
");
?>  
 <style type="text/css">
.text_center{
  text-align:center;
}
</style>
<div class="well">
    <h3>รายการยี่ห้อสินค้า</h3>
 <?php $form=$this->beginWidget('CActiveForm', array(
    'enableAjaxValidation'=>true,
)); ?>
<div style="margin-bottom:-46px;;" class="well">
<?php
echo CHtml::ajaxSubmitButton(
    $label = 'ลบข้อมูล', 
    array('goodsbrand/Ajaxupdate'), array('success' => 'reloadGrid'),
    $htmlOptions=array ('confirm'=>'ยืนยันการลบข้อมูล','class' => 'btn btn-danger')
    );
?>&nbsp;
<?php echo CHtml::link('เพิ่มข้อมูล',array('goodsbrand/create'),array('class'=>'btn btn-info')); ?>  
</div>
<?php 
$alert = 'ยืนยันการลบข้อมูล !';
$this->widget('booster.widgets.TbGridView',array(
'id'=>'goodsbrand-grid',
'type' => 'striped bordered',    
'dataProvider'=>$model->search(),
'filter'=>$model,
'selectableRows'=>3,    
'columns'=>array(
      array(
            'id' => 'id',
            'class' => 'CCheckBoxColumn', 
        ),
     'name',
        array('header' => '<span style="color:#428bca;">ดำเนินการ</span>',
           'class' => 'booster.widgets.TbButtonColumn',
            'template' => '{delete}{update}', //    'template'=>'{add} {list} {update} {print_act}',
            'buttons' => array(
                'update' => array(
                    'label' => 'แก้ไข',
                    'icon' => 'fa fa-pencil-square-o',
                    'url' => 'Yii::app()->controller->createUrl("goodsbrand/update", array("id"=>$data->id))',
                    'options' => array(
                        'class' => 'btn btn-small btn-warning', 'style' => 'margin:5px;',
                    ),
                ),
            'delete'=> array(
                    'url' => 'Yii::app()->controller->createUrl("goodsbrand/delete", array("id"=>$data->id))',
                    'label' => 'ลบ',
                   // 'icon' => 'fa fa-times',
                    'options' => array(// this is the 'html' array but we specify the 'ajax' element
                        'confirm' => $alert,
                        'class' => 'btn btn-small btn-danger',
                         'ajax' => array(
                            'type' => 'POST',
                            'url' => "js:$(this).attr('href')", // ajax post will use 'url' specified above
                            'success' => 'function(data){
                                if(data == 1){ 
                                 alert("ลบข้อมูลเรียบร้อยแล้วค่ะ !"); 
                                  $.fn.yiiGridView.update("goodsbrand-grid");
                                   return false;
                                }else if(data == 2){
                                 alert("คุณต้องลบข้อมูลผู้สมัครที่ลงทะเบียนไว้ก่อนค่ะ");
                                 window.location="index.php?r=admin/goodsbrand/admin";
                                 return false;
                                }else{
                                 alert("การลบข้อมูล error !"); 
                                //   $.fn.yiiGridView.update("goodsbrand-grid");
                                 return false;
                                }
                            }',
                        ),
                    ),
                ), 
            ),
            'htmlOptions' => array(
                'style' => 'width: 200px;', 'class' => 'text_center'
            ),
        )
),
)); ?>
</div>
<script>
function reloadGrid(data) {
    $.fn.yiiGridView.update('goodsbrand-grid');
}
</script>
<?php $this->endWidget(); ?>