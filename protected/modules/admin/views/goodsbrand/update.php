<?php
$this->breadcrumbs=array(
	'Goodsbrands'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List Goodsbrand','url'=>array('index')),
	array('label'=>'Create Goodsbrand','url'=>array('create')),
	array('label'=>'View Goodsbrand','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Goodsbrand','url'=>array('admin')),
	);
	?> 

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>