<?php
$this->breadcrumbs=array(
	'Goodsbrands'=>array('index'),
	$model->name,
);

$this->menu=array(
array('label'=>'List Goodsbrand','url'=>array('index')),
array('label'=>'Create Goodsbrand','url'=>array('create')),
array('label'=>'Update Goodsbrand','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Goodsbrand','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Goodsbrand','url'=>array('admin')),
);
?>

<h1>View Goodsbrand #<?php echo $model->id; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'name',
),
)); ?>
