<?php
$this->breadcrumbs=array(
	'Issuecatergories'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List Issuecatergory','url'=>array('index')),
	array('label'=>'Create Issuecatergory','url'=>array('create')),
	array('label'=>'View Issuecatergory','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Issuecatergory','url'=>array('admin')),
	);
	?> 
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>