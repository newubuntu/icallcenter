<?php
$baseUrl = Yii::app()->getModule('admin')->assetsUrl;
//$baseUrl = Yii::app()->theme->baseUrl;
$cs = Yii::app()->getClientScript(); /*
  $cs->registerScriptFile($baseUrl . '/js/plugins/datatables/jquery.dataTables.js');
  $cs->registerScriptFile($baseUrl . '/js/plugins/datatables/dataTables.bootstrap.js');
  $cs->registerCssFile($baseUrl . '/css/datatables/dataTables.bootstrap.css'); */
$cs->registerScriptFile($baseUrl . '/DataTables/media/js/jquery.dataTables.min.js');
$cs->registerScriptFile($baseUrl . '/DataTables/media/js/dataTables.bootstrap.js');
$cs->registerCssFile($baseUrl . '/DataTables/media/css/dataTables.bootstrap.css');


$getSessionListUrl = $this->createUrl('issue/AjaxAdmin'); 
$sessionLocationAutocompleteUrl = $this->createUrl('SessionLocationAutocomplete');
$personAutocompleteUrl = $this->createUrl('PersonAutocomplete');
$programId = 1; // $model->ID;
Yii::app()->clientScript->registerScript('EditProgram', "

", CClientScript::POS_HEAD);
$this->breadcrumbs = array(
    'Issues' => array('index'),
    'Manage',
);
?>  
<script type="text/javascript">
 $(document).ready(function() {
  var oTable = $('#myDataTable').dataTable({
    "bServerSide": true,
    "sAjaxSource": "<?=$getSessionListUrl?>",
    "bProcessing": true,
    "sPaginationType": "full_numbers",
    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, 'All']],
    "pagingType": "full_numbers",
    "language": {
        "paginate": {
            "first": "หน้าแรก",
            "last": "สุดท้าย",
            "next": "ทัดไป",
            "previous":"ก่อนหน้า",
            "previous": 'ก่อนหน้า',
            "search": "ค้นหา"
         }
       },
    "aoColumns": [{
      "sWidth": "5%", 
      "mData": 0
    }, {
      "sWidth": "40%",
      "mData": 2
    }, {
      "sWidth": "40%",
      "mData": 3
    },{
      "mData": null,
      "sWidth": "20%",
      "bSortable": false,
      "mRender": function(data, type, full) {
        return '<a class="btn btn-info btn-sm" href="<?=$this->createUrl('issue/update')?>&id='+full[0]+'\">' + 'แก้ไข' + '</a>&nbsp;&nbsp;<a onclick="return confirm('+"'ยืนยันการลบข้อมูล'"+');" class="btn btn-danger btn-sm" href="<?=$this->createUrl('issue/delete')?>&id='+full[0]+'\">' + 'ลบ' + '</a>';
      }
    }]
  });

});
 </script>                      
<div class="row">
    <div class="col-xs-12"> 
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">รายการปัญหา</h3>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive">
                <table id="myDataTable" class="table table-striped table-bordered table-hover display">
                    <thead>
                        <tr>
                            <th>ลำดับ</th>
                            <th>หัวข้อปัหญา</th>
                            <th>รายละเอียด</th>
                            <th>ดำเนินการ</th>
                        </tr>
                    </thead>
                    <tbody> 
                    </tbody>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th> 
                        </tr>
                    </tfoot>
                </table>   
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>      
 