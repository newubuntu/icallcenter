<?php
$this->breadcrumbs=array(
	'Issues'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List Issue','url'=>array('index')),
	array('label'=>'Create Issue','url'=>array('create')),
	array('label'=>'View Issue','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Issue','url'=>array('admin')),
	);
	?>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>