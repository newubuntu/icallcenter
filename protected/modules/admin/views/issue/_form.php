<?php
$form = $this->beginWidget('booster.widgets.TbActiveForm', array(
    'id' => 'issue-form',
    'enableAjaxValidation' => false,
    'type' => 'horizontal',
    'htmlOptions' => array('class' => 'well',
        'enctype' => 'multipart/form-data')
        ));
?>
<?php
$mytrue = TRUE;
if (!$model->isNewRecord) {
    $mytrue = FALSE;
}
?>

<fieldset class="myfieldset">
    <?php
    if ($mytrue) {
        ?>
        <legend class="mylegend"><i class="glyphicon glyphicon-plus"></i>เพิ่มรายการปัญหา</legend>
        <?php
    } else {
        ?>
        <legend class="mylegend"><i class="glyphicon glyphicon-pencil"></i>แก้ไขรายการปัญหา&nbsp;<?= cutword::substr_utf8($model->name, 0, 20); ?></legend>    
        <?php
    }
    ?>
    <div style="margin-left:270px;">
        <p class="help-block">โปรดกรอกข้อมูลให้ครบทุกช่อง<span class="required">    *</span> </p>
    </div>
    <?php echo $form->errorSummary($model); ?>
    <?php echo $form->textFieldGroup($model, 'subject', array('widgetOptions' => array('htmlOptions' => array('style' => 'width:450px;')))); ?>
    <?php echo $form->textAreaGroup($model, 'subjectdetail', array('widgetOptions' => array('htmlOptions' => array('rows' => 3, 'style' => 'width:450px;')))); ?>
    <?php echo $form->textAreaGroup($model, 'manage', array('widgetOptions' => array('htmlOptions' => array('rows' => 3, 'style' => 'width:450px;')))); ?>
    <?php echo $form->textAreaGroup($model, 'annotation', array('widgetOptions' => array('htmlOptions' => array('rows' => 3, 'style' => 'width:450px;')))); ?>
    <?php echo $form->dropDownListGroup($model, 'goods_id', array('wrapperHtmlOptions' => array('class' => 'col-sm-5'), 'widgetOptions' => array('data' => Goods::getGoodsAll(), 'htmlOptions' => array('prompt' => 'เลือก' . $model->getAttributeLabel('goods_id'), 'style' => 'width:250px;')))); ?> 
    <?php echo $form->dropDownListGroup($model, 'goodsbrand_id', array('wrapperHtmlOptions' => array('class' => 'col-sm-5'), 'widgetOptions' => array('data' => Goodsbrand::getGoodsbrandAll(), 'htmlOptions' => array('prompt' => 'เลือก' . $model->getAttributeLabel('goodsbrand_id'), 'style' => 'width:250px;')))); ?>
    <?php echo $form->dropDownListGroup($model, 'goodsmodel_id', array('wrapperHtmlOptions' => array('class' => 'col-sm-5'), 'widgetOptions' => array('data' => Goodsmodel::getGoodsmodelAll(), 'htmlOptions' => array('prompt' => 'เลือก' . $model->getAttributeLabel('goodsmodel_id'), 'style' => 'width:250px;')))); ?>
    <div class="form-actions" style="margin-left:280px;">
        <?php
        $this->widget('booster.widgets.TbButton', array(
            'buttonType' => 'submit',
            'context' => 'primary',
            'label' => $model->isNewRecord ? 'บันทึก' : 'แก้ไข',
        ));
        ?>
        <?php
        $this->widget('booster.widgets.TbButton', array(
            'buttonType' => 'reset', 'label' => 'ยกเลิก')
        );
        ?>
    </div>
</fieldset>
<?php $this->endWidget(); ?>