<?php
 $baseUrl=Yii::app()->getModule('admin')->assetsUrl;
 //$baseUrl = Yii::app()->theme->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl . '/js/plugins/datatables/jquery.dataTables.js',CClientScript::POS_END);
$cs->registerScriptFile($baseUrl . '/js/plugins/datatables/dataTables.bootstrap.js',CClientScript::POS_END);
$cs->registerCssFile($baseUrl . '/css/datatables/dataTables.bootstrap.css');
$this->breadcrumbs=array(
	'Issues'=>array('index'),
	'Manage',
);
?>
<?php
Yii::app()->clientScript->registerScript('myuid_datatable', "
             $(function() {  
                $('#example1').dataTable({
                   'processing': true,
                   'serverSide': true,
                    'ajax':'scripts/server_processing.php',
                    'bPaginate': true,
                    'bLengthChange': true,
                    'bFilter': true,
                    'bSort': true,
                    'bInfo': true,
                    'bAutoWidth': true
                });
             });
", CClientScript::POS_END);
?> 
<?php
$this->menu=array(
array('label'=>'List Issue','url'=>array('index')),
array('label'=>'Create Issue','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('issue-grid', {
data: $(this).serialize()
});
return false;
});
");
?> 

<?php
/*
$this->widget('booster.widgets.TbGridView',array(
'id'=>'issue-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'id',
		'subject',
		'subjectdetail',
		'manage',
		'annotation',
		'create_at',
		/*
		'users_id',
		'goodsmodel_id',
		'goodsbrand_id',
		'goods_id',
		 
array(
'class'=>'booster.widgets.TbButtonColumn',
),
),
));*/ ?>
   <div class="row">
                        <div class="col-xs-12"> 
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">รายการปัญหา</h3>
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Rendering engine</th>
                                                <th>Browser</th>
                                                <th>Platform(s)</th>
                                                <th>Engine version</th>
                                                <th>CSS grade</th>
                                            </tr>
                                        </thead>
                                         
                                        <tfoot>
                                            <tr>
                                                <th>Rendering engine</th>
                                                <th>Browser</th>
                                                <th>Platform(s)</th>
                                                <th>Engine version</th>
                                                <th>CSS grade</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>