<?php
$this->breadcrumbs=array(
	'Issues'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List Issue','url'=>array('index')),
array('label'=>'Create Issue','url'=>array('create')),
array('label'=>'Update Issue','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Issue','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Issue','url'=>array('admin')),
);
?>

<h1>View Issue #<?php echo $model->id; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'subject',
		'subjectdetail',
		'manage',
		'annotation',
		'create_at',
		'users_id',
		'goodsmodel_id',
		'goodsbrand_id',
		'goods_id',
),
)); ?>
