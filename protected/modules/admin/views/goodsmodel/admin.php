<?php
$this->breadcrumbs=array(
	'Goodsmodels'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List Goodsmodel','url'=>array('index')),
array('label'=>'Create Goodsmodel','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('goodsmodel-grid', {
data: $(this).serialize()
});
return false;
});
");
?>  
<style type="text/css">
.text_center{
  text-align:center;
}
</style>
<div class="well">
 <h3>รายการรุ่นสินค้า</h3>
<?php // echo CHtml::link('Advanced Search', '#', array('class' => 'search-button btn btn-primary')); ?>
<div class="search-form" style="display:none">
    <?php
    //$this->renderPartial('_search', array(
    //    'model' => $model,
   // ));
    ?>
</div><!-- search-form -->
<?php $form=$this->beginWidget('CActiveForm', array(
    'enableAjaxValidation'=>true,
)); ?>
<div style="margin-bottom:-46px;;" class="well">
<?php
echo CHtml::ajaxSubmitButton(
    $label = 'ลบข้อมูล', 
    array('goodsmodel/Ajaxupdate'), array('success' => 'reloadGrid'),
    $htmlOptions=array ('confirm'=>'ยืนยันการลบข้อมูล','class' => 'btn btn-danger')
    );
?>&nbsp;
<?php echo CHtml::link('เพิ่มข้อมูล',array('goodsmodel/create'),array('class'=>'btn btn-info')); ?>  
</div>
 
<?php
$alert = 'ยืนยันการลบข้อมูล !';

$this->widget('booster.widgets.TbGridView', array(
    'id' => 'goodsmodel-grid',
    'dataProvider' => $model->search(),
    'type' => 'striped bordered',
  //  'rowCssClassExpression'=>'($data->dayclose>$data->getdaynow())?"success":($data->dayclose<$data->getdaynow()?"danger":"")',
    'filter' => $model,
    'selectableRows'=>3,    
    'columns' => array(
        array(
            'id' => 'id',
            'class' => 'CCheckBoxColumn',
           // 'selectableRows' => '50',
        ),
        'name',
	array(
            'name'=>'goodsbrand_id',
            //'header'=>'สถานะ',
            'htmlOptions'=> array( 'class' => 'form-control' ),
	    'filter'=> CHtml::dropDownList( 'goodsmodel[goodsbrand_id]', $model->goodsbrand_id,
	    Goodsbrand::getGoodsbrandAll(),
	    array( 'empty' => '-- ยี่ห้อสินค้า --','class' => 'form-control', )
	    ),
           'value'=>'Goodsbrand::getlebel_Goodsbrand($data->goodsbrand_id)',
            'htmlOptions' => array(
             'style' => 'width: 180px;text-align:center;'//, 'class' => 'form-control'
            ),
	   ), 
        array('header' => '<span style="color:#428bca;">ดำเนินการ</span>',
           'class' => 'booster.widgets.TbButtonColumn',
            'template' => '{delete}{update}', //    'template'=>'{add} {list} {update} {print_act}',
            'buttons' => array(
                'update' => array(
                    'label' => 'แก้ไข',
                    'icon' => 'fa fa-pencil-square-o',
                    'url' => 'Yii::app()->controller->createUrl("goodsmodel/update", array("id"=>$data->id))',
                    'options' => array(
                        'class' => 'btn btn-small btn-warning', 'style' => 'margin:5px;',
                    ),
                ),
            'delete'=> array(
                    'url' => 'Yii::app()->controller->createUrl("goodsmodel/delete", array("id"=>$data->id))',
                    'label' => 'ลบ',
                   // 'icon' => 'fa fa-times',
                    'options' => array(// this is the 'html' array but we specify the 'ajax' element
                        'confirm' => $alert,
                        'class' => 'btn btn-small btn-danger',
                         'ajax' => array(
                            'type' => 'POST',
                            'url' => "js:$(this).attr('href')", // ajax post will use 'url' specified above
                            'success' => 'function(data){
                                if(data == 1){ 
                                 alert("ลบข้อมูลเรียบร้อยแล้วค่ะ !"); 
                                  $.fn.yiiGridView.update("goodsmodel-grid");
                                   return false;
                                }else if(data == 2){
                                 alert("คุณต้องลบข้อมูลผู้สมัครที่ลงทะเบียนไว้ก่อนค่ะ");
                                 window.location="index.php?r=admin/goodsmodel/admin";
                                 return false;
                                }else{
                                 alert("การลบข้อมูล error !"); 
                                //   $.fn.yiiGridView.update("goodsmodel-grid");
                                 return false;
                                }
                            }',
                        ),
                    ),
                ),   
            ),
            'htmlOptions' => array(
                'style' => 'width: 300px;', 'class' => 'text_center'
            ),
        )
    /*
      array(
      'class'=>'booster.widgets.TbButtonColumn',
      ), */
    ),'htmlOptions' => array(
                'style' => 'margin-top:0;'
            ),
   )
  );
?>
</div>
<script>
function reloadGrid(data) {
    $.fn.yiiGridView.update('goodsmodel-grid');
}
</script>
<?php $this->endWidget(); ?>