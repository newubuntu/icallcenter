<?php
$this->breadcrumbs=array(
	'Goodsmodels'=>array('index'),
	$model->name,
);

$this->menu=array(
array('label'=>'List Goodsmodel','url'=>array('index')),
array('label'=>'Create Goodsmodel','url'=>array('create')),
array('label'=>'Update Goodsmodel','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Goodsmodel','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Goodsmodel','url'=>array('admin')),
);
?>

<h1>View Goodsmodel #<?php echo $model->id; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'name',
		'goodsbrand_id',
),
)); ?>
