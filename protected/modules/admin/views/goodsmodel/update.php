<?php
$this->breadcrumbs=array(
	'Goodsmodels'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List Goodsmodel','url'=>array('index')),
	array('label'=>'Create Goodsmodel','url'=>array('create')),
	array('label'=>'View Goodsmodel','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Goodsmodel','url'=>array('admin')),
	);
	?>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>