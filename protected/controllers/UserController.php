<?php

class UserController extends MYAdminController{
	public $layout='//layouts/column1';
	public $layout='main';
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	public function actionCreate()
	{
		$model=new User;
        $model->isactive='yes';
		
		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			$model->isactive=$_POST['User']['isactive'];
			$model->password=md5($_POST['User']['password']);
			$model->createdon=new CDbExpression('NOW()');
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			if(trim($_POST['User']['password'])!='')  {
				$model->password=md5($_POST['User']['password']);
			}
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			//$this->loadModel($id)->delete();
                        User::model()->updateByPk($id,array('isdeleted'=>'yes'));
			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		/*$dataProvider=new CActiveDataProvider('User');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));*/
            
		// page size drop down changed
                if (isset($_GET['pageSize'])) {
                    Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']);
                    unset($_GET['pageSize']);  // would interfere with pager and repetitive page size change
                }
                $model=new User('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['User']))
			$model->attributes=$_GET['User'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new User('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['User']))
			$model->attributes=$_GET['User'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=User::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        public function actionMultipleoperation()
        {
            $fetchIdarr=array();
            $fetchIdStr='';
            $operation=(isset($_POST['operation']) && $_POST['operation']!='') ? $_POST['operation'] : '';
            
            if(isset($_POST['chk']) && count($_POST['chk'])>0) {
                    
                foreach($_POST['chk'] as $val) {
                        $fetchIdarr[]=$val;
                }
                
                $fetchIdStr=implode(',',$fetchIdarr);
            }
           
                
            if($operation=='Disable') {
                
               /* $criteria = new CDbCriteria;
                $criteria->addInCondition( "id" , $fetchIdStr );
                User::model()->updateAll(array('isactive'=>'no'), $criteria);*/
                
               if($fetchIdStr!='') {
                    User::model()->updateAll(array('isactive'=>'no'),'id IN ('.$fetchIdStr.')');
               } 
            }
            
            if($operation=='Enable') {
                
               if($fetchIdStr!='') {
                    User::model()->updateAll(array('isactive'=>'yes'),'id IN ('.$fetchIdStr.')');
               }
            }
            
            
            if($operation=='Delete') {
                
               if($fetchIdStr!='') {
                    User::model()->updateAll(array('isdeleted'=>'yes'),'id IN ('.$fetchIdStr.')');
               }
            }
            
            
        }
        
    
}
