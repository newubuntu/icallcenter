<?php

Yii::app()->getModule('users');
Yii::app()->getModule('mail');
class Controller extends RController
{
    public $layout = '//layouts/main';

    public $breadcrumbs = array();

    public $pageTitle;

    public $mainPage = false;

    public $navigationItemId = null;

    public $portalId = 1;

    private $_assetsBase;

    public function init()
    {
        parent::init();

        if (!$portal = Portal::model()->findByAttributes(array('alias' => $_SERVER['HTTP_HOST'])))
            $portal = Portal::model()->findByPk(1);

        if (Yii::app()->session->get('cart_items') === null)
            Yii::app()->session['cart_items'] = array();

        Yii::app()->clientScript->registerCoreScript('jquery');

        $baseScriptUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')).'/gridview';
        Yii::app()->getClientScript()->registerScriptFile($baseScriptUrl.'/jquery.yiigridview.js');

        $this->portalId = 1;

    }

    public function getAssetsBase()
    {
        if ($this->_assetsBase === null)
        {
            $this->_assetsBase = Yii::app()->assetManager->publish(
                Yii::app()->theme->basePath . '/assets',
                false,
                -1,
                YII_DEBUG
            );
        }

        return $this->_assetsBase;
    }

    /**
     * Редирект на страницу с ошибкой
     * @param $url
     * @param $message
     */
    public function errorTo($url, $message)
    {
        Yii::app()->user->setFlash('error', $message);

        $this->redirect($url);
    }

    /**
     * Редирект на страницу с упешным оповещением
     * @param $url
     * @param $message
     */
    public function noticeTo($url, $message)
    {
        Yii::app()->user->setFlash('notice', $message);
        $this->redirect($url);
    }

      /*
      * To the module worked rights ...
      *return Array
      */
    public function filters()
    {
        return array(
            'rights'
        );
    }

    /**
     * Регистрирует js, css файлы модуля
     * @param array $js
     * @param array $css
     */
    protected function registerModuleAssetsScripts($js = array(), $css = array())
    {
        $clientScript = Yii::app()->getClientScript();

        $modulePath = $this->getModule()->getBasePath();

        $baseUrl = Yii::app()->assetManager->publish("{$modulePath}/assets");

        foreach ($js as $jsFile)
        {
            $clientScript->registerScriptFile("{$baseUrl}/js/{$jsFile}");
        }

        foreach ($css as $cssFile)
        {
            $clientScript->registerCssFile("{$baseUrl}/css/{$cssFile}");
        }
    }
}