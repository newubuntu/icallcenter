<?php

//Yii::app()->getModule('users');
//Yii::app()->getModule('mail');

class Controllers extends RController {

    public $layout = '//layouts/column1';
    public $pageTitle;

    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu = array();
    public $breadcrumbs = array();
    private $_assetsBase;

    public function init() {
        parent::init();
        Yii::app()->clientScript->registerCoreScript('jquery');
        $baseScriptUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')) . '/gridview';
        Yii::app()->getClientScript()->registerScriptFile($baseScriptUrl . '/jquery.yiigridview.js');

        if (Yii::app()->user->isGuest) {
            //   Yii::app()->theme = 'frontend';
        } else {
            //  Yii::app()->theme = 'frontend';
        }
    }

    /*
      ����� * To the module worked rights ...
      ����� *return Array
      ����� */

    /*
      public function filters(){
      return array(
      'rights'
      );
      } */

    /**
     * Redirect to an error page
      ����� *param $ Url
      ����� *param $ Message
     */
    public function errorTo($url, $message) {
        Yii::app()->user->setFlash('error', $message);

        $this->redirect($url);
    }

    public function getAssetsBase() {
        if ($this->_assetsBase === null) {
            $this->_assetsBase = Yii::app()->assetManager->publish(
                    Yii::app()->theme->basePath . '/assets', false, -1, YII_DEBUG
            );
        }

        return $this->_assetsBase;
    }

    /**
      ����� * Registers js, css files module
      ����� *param Array $ js
      ����� *param Array $ css
      ����� */
    protected function registerModuleAssetsScripts($js = array(), $css = array()) {
        $clientScript = Yii::app()->getClientScript();
        $modulePath = $this->getModule()->getBasePath();
        $baseUrl = Yii::app()->assetManager->publish("{$modulePath}/assets");
        foreach ($js as $jsFile) {
            $clientScript->registerScriptFile("{$baseUrl}/js/{$jsFile}");
        }
        foreach ($css as $cssFile) {
            $clientScript->registerCssFile("{$baseUrl}/css/{$cssFile}");
        }
    }

}
