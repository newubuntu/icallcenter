-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 31, 2015 at 01:12 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `icallcenter`
--

-- --------------------------------------------------------

--
-- Table structure for table `authassignment`
--

CREATE TABLE IF NOT EXISTS `authassignment` (
  `itemname` varchar(64) NOT NULL,
  `userid` varchar(64) NOT NULL,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`itemname`,`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `authassignment`
--

INSERT INTO `authassignment` (`itemname`, `userid`, `bizrule`, `data`) VALUES
('Admin', '1', NULL, 'N;'),
('Admin', '4', NULL, 'N;'),
('Authenticated', '2', NULL, 'N;'),
('Moderator', '3', NULL, 'N;');

-- --------------------------------------------------------

--
-- Table structure for table `authitem`
--

CREATE TABLE IF NOT EXISTS `authitem` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `authitem`
--

INSERT INTO `authitem` (`name`, `type`, `description`, `bizrule`, `data`) VALUES
('Admin', 2, 'Administrator', NULL, 'N;'),
('Authenticated', 2, 'authorized', NULL, 'N;'),
('Editor', 2, 'editor', NULL, 'N;'),
('Guest', 2, 'guest', NULL, 'N;'),
('Moderator', 2, 'Moderator', NULL, 'N;');

-- --------------------------------------------------------

--
-- Table structure for table `authitemchild`
--

CREATE TABLE IF NOT EXISTS `authitemchild` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `goods`
--

CREATE TABLE IF NOT EXISTS `goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL COMMENT 'ชื่อสินค้า',
  `annotation` text COMMENT 'หมายเหตุ',
  `active` char(2) DEFAULT '1' COMMENT 'สถานะสินค้า',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='สินค้า' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `goodsbrand`
--

CREATE TABLE IF NOT EXISTS `goodsbrand` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL COMMENT 'ชื่อยี่ห้อ',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ยี่ห้อ' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `goodsmodel`
--

CREATE TABLE IF NOT EXISTS `goodsmodel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL COMMENT 'ชื่อรุ่น',
  `goodsbrand_id` int(11) NOT NULL COMMENT 'ยี่ห้อ',
  PRIMARY KEY (`id`),
  KEY `fk_goodsmodel_goodsbrand1_idx` (`goodsbrand_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='รุ่น' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `goods_has_goodsbrand`
--

CREATE TABLE IF NOT EXISTS `goods_has_goodsbrand` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `goods_id` int(11) NOT NULL,
  `goodsbrand_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_goods_has_goodsbrand_goods1_idx` (`goods_id`),
  KEY `fk_goods_has_goodsbrand_goodsbrand1_idx` (`goodsbrand_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `issue`
--

CREATE TABLE IF NOT EXISTS `issue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` text NOT NULL COMMENT 'แจ้งเรื่อง',
  `subjectdetail` text NOT NULL COMMENT 'รายละเอียดการแจ้ง',
  `manage` text COMMENT 'การดำเนินการ',
  `annotation` text COMMENT 'หมายเหตุ',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'แจ้งเมื่อ',
  `users_id` int(11) NOT NULL,
  `goodsmodel_id` int(11) NOT NULL,
  `goodsbrand_id` int(11) NOT NULL,
  `goods_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_issue_users1_idx` (`users_id`),
  KEY `fk_issue_goodsmodel1_idx` (`goodsmodel_id`),
  KEY `fk_issue_goodsbrand1_idx` (`goodsbrand_id`),
  KEY `fk_issue_goods1_idx` (`goods_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='เก็บปัญหาต่างๆ' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `issuecatergory`
--

CREATE TABLE IF NOT EXISTS `issuecatergory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL COMMENT 'ชื่อหมวดหมู่',
  `active` char(2) NOT NULL DEFAULT '1' COMMENT 'สถานะ',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='หมวดห�' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `issuecatergory_has_knowledge`
--

CREATE TABLE IF NOT EXISTS `issuecatergory_has_knowledge` (
  `issuecatergory_id` int(11) NOT NULL,
  `knowledge_id` int(11) NOT NULL,
  PRIMARY KEY (`issuecatergory_id`,`knowledge_id`),
  KEY `fk_issuecatergory_has_knowledge_knowledge1_idx` (`knowledge_id`),
  KEY `fk_issuecatergory_has_knowledge_issuecatergory1_idx` (`issuecatergory_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `issue_has_issuecatergory`
--

CREATE TABLE IF NOT EXISTS `issue_has_issuecatergory` (
  `issue_id` int(11) NOT NULL,
  `issuecatergory_id` int(11) NOT NULL,
  PRIMARY KEY (`issue_id`,`issuecatergory_id`),
  KEY `fk_issue_has_issuecatergory_issuecatergory1_idx` (`issuecatergory_id`),
  KEY `fk_issue_has_issuecatergory_issue1_idx` (`issue_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `knowledge`
--

CREATE TABLE IF NOT EXISTS `knowledge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` text NOT NULL COMMENT 'หัวข้อเรื่อง',
  `subjectdetail` text NOT NULL COMMENT 'รายละเอียด',
  `active` char(2) NOT NULL DEFAULT '1' COMMENT 'สถานะ',
  `users_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_knowledge_users1_idx` (`users_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='คลังความรู้' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE IF NOT EXISTS `profiles` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `birthday` date DEFAULT NULL,
  `avatar` text,
  `lastname` varchar(50) NOT NULL DEFAULT '',
  `firstname` varchar(50) NOT NULL DEFAULT '',
  `status` text,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`user_id`, `birthday`, `avatar`, `lastname`, `firstname`, `status`) VALUES
(1, '0000-00-00', '51f27f02f2808.jpg', 'Горшков', 'Александр', ''),
(2, '0000-00-00', '51f19447c11f6.jpg', 'Безобразова', 'Ирина', 'Удачных выходных)'),
(3, '0000-00-00', '', 'Гагатунов', 'Гагатун', ''),
(4, '0000-00-00', '', 'Sakharova', 'Anna', '');

-- --------------------------------------------------------

--
-- Table structure for table `profiles_fields`
--

CREATE TABLE IF NOT EXISTS `profiles_fields` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `varname` varchar(50) NOT NULL,
  `title` varchar(255) NOT NULL,
  `field_type` varchar(50) NOT NULL,
  `field_size` varchar(15) NOT NULL DEFAULT '0',
  `field_size_min` varchar(15) NOT NULL DEFAULT '0',
  `required` int(1) NOT NULL DEFAULT '0',
  `match` varchar(255) NOT NULL DEFAULT '',
  `range` varchar(255) NOT NULL DEFAULT '',
  `error_message` varchar(255) NOT NULL DEFAULT '',
  `other_validator` varchar(5000) NOT NULL DEFAULT '',
  `default` varchar(255) NOT NULL DEFAULT '',
  `widget` varchar(255) NOT NULL DEFAULT '',
  `widgetparams` varchar(5000) NOT NULL DEFAULT '',
  `position` int(3) NOT NULL DEFAULT '0',
  `visible` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `varname` (`varname`,`widget`,`visible`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `profiles_fields`
--

INSERT INTO `profiles_fields` (`id`, `varname`, `title`, `field_type`, `field_size`, `field_size_min`, `required`, `match`, `range`, `error_message`, `other_validator`, `default`, `widget`, `widgetparams`, `position`, `visible`) VALUES
(2, 'avatar', 'รูป', 'VARCHAR', '255', '0', 0, '', '', '', '{"file":{"allowEmpty":"true"}}', '', 'UWfile', '{"path":"uploads/avatars"}', 0, 2),
(3, 'lastname', 'นามสกุล', 'VARCHAR', '255', '0', 1, '', '', '', '', '', '', '', 0, 3),
(4, 'firstname', 'ชื่อ', 'VARCHAR', '255', '0', 1, '', '', '', '', '', '', '', 0, 3),
(5, 'status', 'สถานะ', 'VARCHAR', '255', '0', 0, '', '', '', '', '', '', '', 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `rights`
--

CREATE TABLE IF NOT EXISTS `rights` (
  `itemname` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `weight` int(11) NOT NULL,
  PRIMARY KEY (`itemname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rights`
--

INSERT INTO `rights` (`itemname`, `type`, `weight`) VALUES
('Admin', 2, 0),
('Authenticated', 2, 3),
('Editor', 2, 2),
('Guest', 2, 4),
('Moderator', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `userinform`
--

CREATE TABLE IF NOT EXISTS `userinform` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lname` varchar(105) NOT NULL COMMENT 'ชื่อ',
  `fname` varchar(105) DEFAULT NULL COMMENT 'นามสกุล',
  `email` varchar(105) DEFAULT NULL COMMENT 'อีเมลล์',
  `to` varchar(15) DEFAULT 'NUL' COMMENT 'เบอร์โทร',
  `issue_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_userinform_issue_idx` (`issue_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ผู้แจ้ง' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `activkey` varchar(128) NOT NULL DEFAULT '',
  `createtime` timestamp NULL DEFAULT NULL,
  `lastvisit` timestamp NULL DEFAULT NULL,
  `superuser` int(1) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  KEY `status` (`status`),
  KEY `superuser` (`superuser`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `activkey`, `createtime`, `lastvisit`, `superuser`, `status`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'webmaster@example.com', '9a24eff8c15a6a141ece27eb6947da0f', '2015-01-21 16:00:23', '0000-00-00 00:00:00', 1, 1),
(2, 'demo', 'fe01ce2a7fbac8fafaed7c982a04e229', 'demo@example.com', '099f825543f7850cc038b90aaff39fac', '2015-01-20 16:00:23', '0000-00-00 00:00:00', 0, 1),
(3, 'ubuntu', '4130655f3865cae2ea81c878c2533c4f', 'test@gmail.com', '3c9b17f3d81e371fdf5ed7ffa89da481', '2015-01-21 03:00:00', '2015-01-21 03:00:00', 0, 0),
(4, 'newubuntu', '4130655f3865cae2ea81c878c2533c4f', 'test2@gmail.com', 'd017fc1a5ef6a35f3cf6924196ae783f', '2015-01-21 03:00:00', '2015-01-21 03:00:00', 0, 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `authassignment`
--
ALTER TABLE `authassignment`
  ADD CONSTRAINT `authassignment_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `authitemchild`
--
ALTER TABLE `authitemchild`
  ADD CONSTRAINT `authitemchild_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `authitemchild_ibfk_2` FOREIGN KEY (`child`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `goodsmodel`
--
ALTER TABLE `goodsmodel`
  ADD CONSTRAINT `fk_goodsmodel_goodsbrand1` FOREIGN KEY (`goodsbrand_id`) REFERENCES `goodsbrand` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `goods_has_goodsbrand`
--
ALTER TABLE `goods_has_goodsbrand`
  ADD CONSTRAINT `fk_goods_has_goodsbrand_goods1` FOREIGN KEY (`goods_id`) REFERENCES `goods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_goods_has_goodsbrand_goodsbrand1` FOREIGN KEY (`goodsbrand_id`) REFERENCES `goodsbrand` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `issue`
--
ALTER TABLE `issue`
  ADD CONSTRAINT `fk_issue_goods1` FOREIGN KEY (`goods_id`) REFERENCES `goods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_issue_goodsbrand1` FOREIGN KEY (`goodsbrand_id`) REFERENCES `goodsbrand` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_issue_goodsmodel1` FOREIGN KEY (`goodsmodel_id`) REFERENCES `goodsmodel` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_issue_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `issuecatergory_has_knowledge`
--
ALTER TABLE `issuecatergory_has_knowledge`
  ADD CONSTRAINT `fk_issuecatergory_has_knowledge_issuecatergory1` FOREIGN KEY (`issuecatergory_id`) REFERENCES `issuecatergory` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_issuecatergory_has_knowledge_knowledge1` FOREIGN KEY (`knowledge_id`) REFERENCES `knowledge` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `issue_has_issuecatergory`
--
ALTER TABLE `issue_has_issuecatergory`
  ADD CONSTRAINT `fk_issue_has_issuecatergory_issue1` FOREIGN KEY (`issue_id`) REFERENCES `issue` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_issue_has_issuecatergory_issuecatergory1` FOREIGN KEY (`issuecatergory_id`) REFERENCES `issuecatergory` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `knowledge`
--
ALTER TABLE `knowledge`
  ADD CONSTRAINT `fk_knowledge_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `profiles`
--
ALTER TABLE `profiles`
  ADD CONSTRAINT `user_profile_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `rights`
--
ALTER TABLE `rights`
  ADD CONSTRAINT `rights_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `userinform`
--
ALTER TABLE `userinform`
  ADD CONSTRAINT `fk_userinform_issue` FOREIGN KEY (`issue_id`) REFERENCES `issue` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
